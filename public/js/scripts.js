
(function($) {
    "use strict";

    /*================================
    Preloader
    ==================================*/

    var preloader = $('#preloader');
    $(window).on('load', function() {
        setTimeout(function() {
            preloader.fadeOut('slow', function() { $(this).remove(); });
        }, 300)
    });

    /*================================
    sidebar collapsing
    ==================================*/
    if (window.innerWidth <= 1364) {
        $('.page-container').addClass('sbar_collapsed');
    }
    $('.nav-btn').on('click', function() {
        $('.page-container').toggleClass('sbar_collapsed');
    });

    /*================================
    Start Footer resizer
    ==================================*/
    var e = function() {
        var e = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 5;
        (e -= 67) < 1 && (e = 1), e > 67 && $(".main-content").css("min-height", e + "px")
    };
    $(window).ready(e), $(window).on("resize", e);

    /*================================
    sidebar menu
    ==================================*/
    $("#menu").metisMenu();

    /*================================
    slimscroll activation
    ==================================*/
    $('.menu-inner').slimScroll({
        height: 'auto'
    });
    $('.nofity-list').slimScroll({
        height: '435px'
    });
    $('.timeline-area').slimScroll({
        height: '500px'
    });
    $('.recent-activity').slimScroll({
        height: 'calc(100vh - 114px)'
    });
    $('.settings-list').slimScroll({
        height: 'calc(100vh - 158px)'
    });

    /*================================
    stickey Header
    ==================================*/
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop(),
            mainHeader = $('#sticky-header'),
            mainHeaderHeight = mainHeader.innerHeight();

        // console.log(mainHeader.innerHeight());
        if (scroll > 1) {
            $("#sticky-header").addClass("sticky-menu");
        } else {
            $("#sticky-header").removeClass("sticky-menu");
        }
    });

    /*================================
    form bootstrap validation
    ==================================*/
    $('[data-toggle="popover"]').popover()

    /*------------- Start form Validation -------------*/
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    /*================================
    datatable active
    ==================================*/
    if ($('#dataTable').length) {
        $('#dataTable').DataTable({
            responsive: true
        });
    }
    if ($('#dataTable2').length) {
        $('#dataTable2').DataTable({
            responsive: true
        });
    }
    if ($('#dataTable3').length) {
        $('#dataTable3').DataTable({
            responsive: true
        });
    }


    /*================================
    Slicknav mobile menu
    ==================================*/
    $('ul#nav_menu').slicknav({
        prependTo: "#mobile_menu"
    });

    /*================================
    login form
    ==================================*/
    $('.form-gp input').on('focus', function() {
        $(this).parent('.form-gp').addClass('focused');
    });
    $('.form-gp input').on('focusout', function() {
        if ($(this).val().length === 0) {
            $(this).parent('.form-gp').removeClass('focused');
        }
    });

    /*================================
    slider-area background setting
    ==================================*/
    $('.settings-btn, .offset-close').on('click', function() {
        $('.offset-area').toggleClass('show_hide');
        $('.settings-btn').toggleClass('active');
    });

    /*================================
    Owl Carousel
    ==================================*/
    function slider_area() {
        var owl = $('.testimonial-carousel').owlCarousel({
            margin: 50,
            loop: true,
            autoplay: false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                450: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1000: {
                    items: 2
                },
                1360: {
                    items: 1
                },
                1600: {
                    items: 2
                }
            }
        });
    }
    slider_area();

    /*================================
    Fullscreen Page
    ==================================*/

    if ($('#full-view').length) {

        var requestFullscreen = function(ele) {
            if (ele.requestFullscreen) {
                ele.requestFullscreen();
            } else if (ele.webkitRequestFullscreen) {
                ele.webkitRequestFullscreen();
            } else if (ele.mozRequestFullScreen) {
                ele.mozRequestFullScreen();
            } else if (ele.msRequestFullscreen) {
                ele.msRequestFullscreen();
            } else {
                console.log('Fullscreen API is not supported.');
            }
        };

        var exitFullscreen = function() {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else {
                console.log('Fullscreen API is not supported.');
            }
        };

        var fsDocButton = document.getElementById('full-view');
        var fsExitDocButton = document.getElementById('full-view-exit');

        fsDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            requestFullscreen(document.documentElement);
            $('body').addClass('expanded');
        });

        fsExitDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            exitFullscreen();
            $('body').removeClass('expanded');
        });
    }

})(jQuery);

/*================================
Preloader
================================*/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    // Add Row
    let rowCount = 0;
    $('#add-row-btn').on('click', function() {
        var newRow = `
            <tr>
            <td>${rowCount++}</td>
            <td><input class="input-name" type="text" name="list[${rowCount}][name]"><div class="error error-name" name="listname[${rowCount}]error"></td>
            <td><input class="input-phone" type="text" name="list[${rowCount}][phone]"><div class="error error-phone" name="listphone[${rowCount}]error"></td>
            <td><input class="input-date" type="date" name="list[${rowCount}][date]"><div class="error error-date" name="listdate[${rowCount}]error"></td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td>dsfsfsdf</td>
            <td><a class="btn-remove"><img src="images/icon/remove.png" alt="" width="20px"></a></td>
            </tr>
        `;
        $('#data-table tbody').append(newRow);
        updateRowNumbers(); 
        updateRowNumbers2();
    });

    // Remove Row
    $(document).on('click', '.btn-remove', function() {
      $(this).closest('tr').remove();
      updateRowNumbers();
      updateRowNumbers2();
    });

    // Update Row
    function updateRowNumbers() {
        var rows = $('#data-table tbody tr');
        rows.each(function(index) {
            $(this).find('td:first').text(index + 1);
            $(this).find('.input-name').attr('name', `list[${index}][name]`);
            $(this).find('.input-phone').attr('name', `list[${index}][phone]`);
            $(this).find('.input-date').attr('name', `list[${index}][date]`);
        });
    }

    function updateRowNumbers2() {
        var rows = $('#data-table tbody tr');
        rows.each(function(index) {
          $(this).find('input[name^="list"]').each(function() {
            var name = $(this).attr('name');
            $(this).attr('name', name.replace(/\[(\d+)\]/, '[' + index + ']'));
          });
          $(this).find('div[name^="list"]').each(function() {
            var name = $(this).attr('name');
            $(this).attr('name', name.replace(/\[(\d+)\]/, '[' + index + ']'));
          });
        });
      }      

    function handleErrors(errors) {
        $('.error').html('');
      
        $.each(errors, function(key, value) {
            var match = key.match(/list\.(\d+)\.(.*)/);
            if (key === 'email') {
                $('#email-error').append('<div class="alert alert-danger">' + value + '</div>');
            } else if (key === 'phone') {
                $('#phone-error').append('<div class="alert alert-danger">' + value + '</div>');
            }else if (key === 'num') {
                $('#num-error').append('<div class="alert alert-danger">' + value + '</div>');
            }else if (key === 'namefile') {
                $('#namefile-error').append('<div class="alert alert-danger">' + value + '</div>');
            }else if (key === 'date') {
                $('#date-error').append('<div class="alert alert-danger">' + value + '</div>');
            }
            
            if (match) {
                var index = parseInt(match[1]);
                var field = match[2];
                if (field === 'date') {
                    $('[name="listdate[' + index + ']error"]').html('<div class="alert alert-danger">' + value + '</div>');
                    } else if (field === 'name') {
                    $('[name="listname[' + index + ']error"]').html('<div class="alert alert-danger">' + value + '</div>');
                    } else if (field === 'phone') {
                    $('[name="listphone[' + index + ']error"]').html('<div class="alert alert-danger">' + value + '</div>');
                    }                         
            }               
        });
      }
      
    
    function senData() {
        var formData = new FormData($("#form-data")[0]);
        $.ajax({
            url: 'send-data',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                alert(data);
            },
            error: function (xhr) {
                var errors = xhr.responseJSON.errors;
                handleErrors(errors);
            },
        });
    }

    // Save Data
    $('#display-btn').on('click', function() {
      senData();
    });

    $('#display-btn2').on('click', function(event){
        event.preventDefault();
       senData();
    });

    document.addEventListener('keydown', function(event){
        if(event.key === 'F12'){
            event.preventDefault();
            senData();
        }
    });

   
    
});
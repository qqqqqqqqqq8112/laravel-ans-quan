<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Bootstrap 4 Admin Dashboard</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/png" href="{{ url('images/icon/favicon.ico') }}">
    
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('css/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ url('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/slicknav.min.css') }}">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="{{ url('css/typography.css') }}">
    <link rel="stylesheet" href="{{url('css/default-css.css')}}">

    <link rel="stylesheet" href="{{ url('css/styles.css') }}">
    <link rel="stylesheet" href="{{ url('css/responsive.css') }}">
    <!-- modernizr css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="{{ url('/') }}"><img src="{{asset('images/icon/logo.png')}}" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-home"><span id="word-sidebar"> Dashbroad</span></i></a>
                            </li>   
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-list"><span id="word-sidebar"> List</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-shopping-cart"><span id="word-sidebar"> Cart</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-hdd-o"><span id="word-sidebar"> Store</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-archive"><span id="word-sidebar"> Archive</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-line-chart"><span id="word-sidebar"> Chart</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-database"><span id="word-sidebar"> Database</span></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-cog"><span id="word-sidebar"> Setting</span></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                       
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-6 col-sm-4 clearfix">
                        <ul class="notification-area pull-right">
                            <li class="dropdown">
                                <i class="ti-bell dropdown-toggle" data-toggle="dropdown">
                                    <span>3</span>
                                </i>
                                <div class="dropdown-menu bell-notify-box notify-box">
                                    <span class="notify-title">You have 3 new notifications <a href="#">view all</a></span>
                                    <div class="nofity-list">
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                            <div class="notify-text">
                                                <p>You have Changed Your Password</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-comments-smiley btn-info"></i></div>
                                            <div class="notify-text">
                                                <p>New Commetns On Post</p>
                                                <span>30 Seconds ago</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-primary"></i></div>
                                            <div class="notify-text">
                                                <p>Some special like you</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-comments-smiley btn-info"></i></div>
                                            <div class="notify-text">
                                                <p>New Commetns On Post</p>
                                                <span>30 Seconds ago</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-primary"></i></div>
                                            <div class="notify-text">
                                                <p>Some special like you</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                            <div class="notify-text">
                                                <p>You have Changed Your Password</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                        <a href="#" class="notify-item">
                                            <div class="notify-thumb"><i class="ti-key btn-danger"></i></div>
                                            <div class="notify-text">
                                                <p>You have Changed Your Password</p>
                                                <span>Just Now</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <div class="user-profile pull-right">
                                <img class="avatar user-thumb" src="{{ asset('images/author/avatar.png')}}" alt="avatar">
                                <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Kumkum Rai <i class="fa fa-angle-down"></i></h4>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Message</a>
                                    <a class="dropdown-item" href="#">Settings</a>
                                    <a class="dropdown-item" href="#">Log Out</a>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left"><strong>だれひとりとして</strong></h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{ url('/') }}">Home</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <p>Startdate: 2022/01/01 15:30 | Enddate: 2022/01/01 16:30 | あるちいさ:いうむすめ</p>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- basic table start -->
                    <form id="form-data">
                        <div class="col">
                            <div class="card">
                                <div class="bar-search">
                                    <div class="row info">
                                        <div class="col-6 col-md-4">
                                            <div>
                                                <label class="form-label" for="form1">Name File</label>
                                            </div>
                                            <div class="input-group">
                                                <input id="name-file" type="text" name="namefile" class="form-control" />
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-primary">
                                                        <i class="fa fa-search"></i>
                                                    </button> 
                                                </div>
                                            </div> 
                                            <div id="namefile-error" class="error error-message"></div>                    
                                            <div>
                                                <label class="form-label-1" for="form1">Chose file</label>
                                            </div>
                                            <div class="form-outline">
                                                <input id="file" type="file" name="file" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div>
                                                <label>Number</label>
                                            </div>
                                            <div class="form-outline">
                                                <input type="text" name="num" class="form-control"/>
                                                <div id="num-error" class="error error-message"></div>
                                            </div>                                     
                                            <div>
                                                <label>Date</label>
                                            </div>
                                            <div class="form-outline">
                                                <input type="date" name="date" class="form-control" />
                                                <div id="date-error" class="error error-message"></div>
                                            </div>                                  
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <div>
                                                <label>Email</label>
                                            </div>
                                            <div class="form-outline">
                                                <input type="text" name="email" class="form-control" />
                                                <div id="email-error" class="error error-message"></div>
                                            </div>                                      
                                            <div>
                                                <label>Tel</label>
                                            </div>
                                            <div class="form-outline">
                                                <input type="text" name="phone" class="form-control" />
                                                <div id="phone-error" class="error error-message"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-get">
                                    <button type="button" id="display-btn">Get data</button>
                                </div>
                                <hr class="new1">
                                <div class="card-body">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i><strong> Edit Table</strong>
                                    <div class="single-table">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="data-table">
                                                <thead>
                                                    <tr>
                                                        <th><a class="add-btn" id="add-row-btn"><img src="{{asset('images/icon/add-button.png')}}" alt="" width="12px"></a></th>
                                                        <th>Name</th>
                                                        <th>Phone</th>
                                                        <th>Date</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>dasdsdsasd</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input class="input-name" type="text" name="list[0][name]"><div class="error error-name" name="listname[0]error"></div></td>
                                                        <td><input class="input-phone" type="text" name="list[0][phone]"><div class="error error-phone" name="listphone[0]error"></div></td>
                                                        <td><input class="input-date" type="date" name="list[0][date]"><div class="error error-date" name="listdate[0]error"></div></td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td>dsfsfsdf</td>
                                                        <td><a class="btn-remove"><img src="{{asset('images/icon/remove.png')}}" alt="" width="20px"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- basic table end -->
                </div>
            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <a href="javascript:void(0)"><i class="fa fa-angle-double-left" aria-hidden="true"></i> ESC </a>
                <div class="btn-f">
                    <button type="button" class="btn btn-primary btn-sm">F1</button>
                    <button type="button" class="btn btn-primary btn-sm">F2</button>
                    <button type="button" class="btn btn-primary btn-sm">F3</button>
                    <button type="button" class="btn btn-primary btn-sm">F4</button>
                </div>
                <div class="btn-f1">
                    <button type="button" class="btn btn-primary btn-sm">F5</button>
                    <button type="button" class="btn btn-primary btn-sm">F6</button>
                    <button type="button" class="btn btn-primary btn-sm">F7</button>
                    <button type="button" class="btn btn-primary btn-sm">F8</button>
                </div>
                <div class="btn-f2">
                    <button type="button" class="btn btn-primary btn-sm">F9</button>
                    <button type="button" class="btn btn-primary btn-sm">F10</button>
                    <button type="button" class="btn btn-primary btn-sm">F11</button>
                    <button type="button" class="btn btn-primary btn-sm" id="display-btn2">F12</button>
                </div>
            </div>
        </footer>
        <!-- footer area end-->
    </div>
    <!-- page container area end -->
    <!-- offset area start -->
    
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="{{url('js/vendor/jquery-2.2.4.min.js')}}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{url('js/popper.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/owl.carousel.min.js')}}"></script>
    <script src="{{url('js/metisMenu.min.js')}}"></script>
    <script src="{{url('js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{url('js/jquery.slicknav.min.js')}}"></script>

    <!-- others plugins -->
    <script src="{{url('js/plugins.js')}}"></script>
    <script src="{{url('js/scripts.js')}}"></script>
</body>
</html>
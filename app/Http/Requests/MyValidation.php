<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MyValidation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required|regex:/[0-9]{10,11}/',
            'num' => 'required|numeric',
            'namefile' => 'required|string|max:255',
            'date' => 'required|date',
            'list.*.name' => 'required|string|max:255',
            'list.*.phone' => 'required|regex:/[0-9]{10,11}/',
            'list.*.date' => 'required|date',
        ];
    }
    public function messages()
    {
        return [
            'phone.required' => 'The phone field is required.',
            'phone.min' => 'The phone field must be at least 10 digits.',
            'phone.max' => 'The phone field may not be greater than 11 digits.',
            'num.required' => 'The num field is required.',
            'num.numeric' => 'The num field must be a numeric value.',
            'email.required' => 'The email field is required.',
            'email.email' => 'The email field must be a valid email address.',
            'namefile.required' => 'The namefile field is required.',
            'namefile.string' => 'The namefile field must be a string.',
            'date.required' => 'The date field is required.',
            'date.date' => 'The date field must be a valid date.',
            'list.*.name.required' => 'The list name field is required.',
            'list.*.phone.required' => 'The list phone field is required.',
            'list.*.date.required' => 'The list date field is required.',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MyValidation;

class SendDataController extends Controller
{
    public function sendData(MyValidation $request){  
        $request->all();
        return redirect('/index');
    }
}
